1.
select distinct role_name from m_roles;
2.
select username AS first_name, surname AS sure_name, count(user_id) AS count_cars from m_users
inner join m_cars ON m_users.id = m_cars.user_id
GROUP BY username, surname;
3.
select name , count(model) from  m_auto_dealer
inner join m_cars mc on m_auto_dealer.id = mc.dealer_id
inner join m_body mb on mc.id = mb.car_id
where mb.color = 'red' and mb.created > to_timestamp('01-01-2018 00:00:00', 'dd-mm-yyyy hh24:mi:ss')
GROUP BY name;
4.
select username, surname from m_users mu
inner join l_user_location lul on mu.id = lul.user_id
inner join m_cars mc on mu.id = mc.user_id
inner join m_body mb on mc.id = mb.car_id
inner join m_engine me on mc.id = me.car_id
inner join l_car_location lcl on mc.id = lcl.m_car_id
inner join m_auto_dealer mad on mc.dealer_id = mad.id
inner join l_dealer_location ldl on mad.id = ldl.m_auto_dealer_id
WHERE lul.m_location_id !=1 and lul.m_location_id !=5 and lcl.m_location_id = 4 and ldl.m_location_id !=4
and me.volume > 3 and mb.created > to_timestamp('01-01-2010 00:00:00', 'dd-mm-yyyy hh24:mi:ss')
and mb.created < to_timestamp('31-12-2015 00:00:00', 'dd-mm-yyyy hh24:mi:ss');
5.
select login from m_users
inner join m_cars mc on m_users.id = mc.user_id
GROUP BY login
having count(user_id) > 3;
6.
select distinct name, sum(price) from m_auto_dealer
inner join m_cars mc on m_auto_dealer.id = mc.dealer_id
GROUP BY name;
7.
select distinct count(username) from m_users
inner join m_cars mc on m_users.id = mc.user_id
where price > (select avg(price) from m_cars inner join m_users mu on m_cars.user_id = mu.id)