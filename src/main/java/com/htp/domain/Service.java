package com.htp.domain;

import java.util.Objects;

public class Service {

    private Long id;

    private String serviceName;

    private String serviceAddress;

    public Service() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Service)) return false;
        Service service = (Service) o;
        return id.equals(service.id) &&
                serviceName.equals(service.serviceName) &&
                serviceAddress.equals(service.serviceAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, serviceName, serviceAddress);
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", serviceName='" + serviceName + '\'' +
                ", serviceAddress='" + serviceAddress + '\'' +
                '}';
    }
}
