package com.htp.domain;

import java.util.Objects;

public class Tools {

    private Long id;

    private String brand;

    private String model;

    private String personalNumber;

    private Double price;

    private boolean availability;

    public Tools() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tools)) return false;
        Tools tools = (Tools) o;
        return availability == tools.availability &&
                id.equals(tools.id) &&
                brand.equals(tools.brand) &&
                model.equals(tools.model) &&
                personalNumber.equals(tools.personalNumber) &&
                price.equals(tools.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, model, personalNumber, price, availability);
    }

    @Override
    public String toString() {
        return "Tools{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", personalNumber='" + personalNumber + '\'' +
                ", price=" + price +
                ", availability=" + availability +
                '}';
    }
}
