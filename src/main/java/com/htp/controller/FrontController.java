package com.htp.controller;

import com.htp.dao.ServiceDao;
import com.htp.dao.ServiceDaoImpl;
import com.htp.dao.ToolsDao;
import com.htp.dao.ToolsDaoImpl;
import com.htp.domain.Service;
import com.htp.domain.Tools;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Collectors;

public class FrontController extends HttpServlet {

    public static final String FIND_ONE = "findOne";
    public static final String FIND_BY_ID = "findById";
    public static final String FIND_ALL = "findAll";
    public static final String SAVE = "save";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String TEST_BATCH = "batch";


    private ToolsDao toolsDao = new ToolsDaoImpl();
    private ServiceDao serviceDao = new ServiceDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String searchQuery = StringUtils.isNotBlank(req.getParameter("id")) ? req.getParameter("id") : "0";
        String typeOfSearch = StringUtils.isNotBlank(req.getParameter("type")) ? req.getParameter("type") : "0";
        String toolsBrand = StringUtils.isNotBlank(req.getParameter("toolsBrand")) ? req.getParameter("toolsBrand") : "0";
        String tableName = StringUtils.isNotBlank(req.getParameter("table")) ? req.getParameter("table") : "0";

        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/bye");
            if (dispatcher != null) {

                String result = "";

                switch (typeOfSearch) {
                    case FIND_ONE:
                        if (tableName.equals("service")) {

                            result = serviceDao.findOne(Long.parseLong(searchQuery)).getServiceName();
                        } else if (tableName.equals("tools")) {
                            result = toolsDao.findOne(Long.parseLong(searchQuery)).getBrand();
                        }

                        break;
                    case FIND_BY_ID:
                        //result = userDao.findById(Long.parseLong(searchQuery)).orElseThrow(ResourceNotFoundException::new).getLogin();
                        if (tableName.equals("service")) {
                            Optional<Service> serviceOptional = serviceDao.findById(Long.parseLong(searchQuery));
                            if (serviceOptional.isPresent()) {
                                result = serviceOptional.get().getServiceName();
                            }
                        } else if (tableName.equals("tools")) {
                            Optional<Tools> toolsOptional = toolsDao.findById(Long.parseLong(searchQuery));
                            if (toolsOptional.isPresent()) {
                                result = toolsOptional.get().getBrand();
                            }
                        }
                        break;
                    case SAVE:
                        if (tableName.equals("service")) {
                            Service service = new Service();
                            /*5. Columns mapping*/
                            service.setServiceName("new Service");
                            service.setServiceAddress("New Service Address");

                            result = serviceDao.save(service).getServiceName();
                        } else if (tableName.equals("tools")) {
                            Tools tools = new Tools();
                            /*5. Columns mapping*/
                            tools.setBrand(toolsBrand);
                            tools.setModel(toolsBrand);
                            tools.setPersonalNumber("41/16");
                            tools.setPrice(2.00);
                            tools.setAvailability(true);

                            result = toolsDao.save(tools).getBrand();
                        }
                        break;
                    case UPDATE:
                        if (tableName.equals("service")) {

                            Service serviceForUpdate = serviceDao.findOne(Long.parseLong(searchQuery));
                            serviceForUpdate.setServiceName("New updated Name");

                            result = serviceDao.update(serviceForUpdate).getServiceName();

                        } else if (tableName.equals("tools")) {
                            Tools toolsForUpdate = toolsDao.findOne(Long.parseLong(searchQuery));
                            toolsForUpdate.setBrand(toolsBrand);

                            result = toolsDao.update(toolsForUpdate).getBrand();
                        }
                        break;
                    case DELETE:
                        if (tableName.equals("service")) {
                            Service serviceForDelete = serviceDao.findOne(Long.parseLong(searchQuery));

                            result = String.valueOf(serviceDao.delete(serviceForDelete));
                        } else if (tableName.equals("tools")) {
                            Tools toolsForDelete = toolsDao.findOne(Long.parseLong(searchQuery));

                            result = String.valueOf(toolsDao.delete(toolsForDelete));
                        }

                        break;
                    case TEST_BATCH:
                        Tools tools = new Tools();
                        /*5. Columns mapping*/
                        tools.setBrand(toolsBrand);
                        tools.setModel(toolsBrand);
                        tools.setPersonalNumber("41/16");
                        tools.setPrice(2.00);
                        tools.setAvailability(true);

                        Tools toolsTwo = new Tools();
                        toolsTwo.setBrand(toolsBrand);
                        toolsTwo.setModel(toolsBrand);
                        toolsTwo.setPersonalNumber("41/16.2");
                        toolsTwo.setPrice(5.2);
                        toolsTwo.setAvailability(false);

                        result = toolsDao.testBatch(tools, toolsTwo).stream().map(Tools::getBrand).collect(Collectors.joining(","));
                        break;
                    case FIND_ALL:
                    default:
                        if (tableName.equals("service")) {

                            result = serviceDao.findAll().stream().map(Service::getServiceName).collect(Collectors.joining(","));
                        } else if (tableName.equals("tools")) {

                            result = toolsDao.findAll().stream().map(Tools::getBrand).collect(Collectors.joining(","));
                        }

                        break;
                }

                req.setAttribute("toolsBrands", result);
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/error");
            req.setAttribute("errors", e.getMessage());
            dispatcher.forward(req, resp);
        }
    }
}

