package com.htp.dao;

import com.htp.domain.Service;

import java.util.List;
import java.util.Optional;

public interface ServiceDao {

    List<Service> findAll();

    List<Service> search(String searchParam);

    Optional<Service> findById(Long serviceId);

    Service findOne(Long serviceId);

    Service save(Service service);

    Service update(Service service);

    int delete(Service service);

    List<Service> testBatch(Service serviceOne, Service serviceTwo);

}
