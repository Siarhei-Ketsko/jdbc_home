package com.htp.dao;

import com.htp.domain.Tools;
import com.htp.exceptions.ResourceNotFoundException;
import com.htp.util.DatabaseConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.htp.util.DatabaseConfiguration.*;
import static com.htp.util.DatabaseConfiguration.DATABASE_PASSWORD;

public class ToolsDaoImpl implements ToolsDao {
    public static DatabaseConfiguration config = DatabaseConfiguration.getInstance();

    public static final String TOOLS_ID = "id";
    public static final String TOOLS_BRAND = "brand";
    public static final String TOOLS_MODEL = "model";
    public static final String TOOLS_PERSONAL_NUMBER = "personal_number";
    public static final String TOOLS_PRICE = "price";
    public static final String TOOLS_AVAILABILITY = "availability";


    @Override
    public List<Tools> findAll() {
        final String findAllQuery = "select * from m_tools order by id desc";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        List<Tools> resultList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);

             PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return resultList;
    }

    @Override
    public List<Tools> search(String searchParam) {
        final String findAllQueryForPrepared = "select * from m_tools where id > ? order by id desc";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        List<Tools> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared)) {

            preparedStatement.setLong(1, Long.parseLong(searchParam));

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public Optional<Tools> findById(Long toolsId) {

        return Optional.ofNullable(findOne(toolsId));
    }

    public Tools findOne(Long toolsId) {
        final String findById = "select * from m_tools where id = ? ";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        Tools tools = null;
        ResultSet resultSet = null;
        /*2. DriverManager should get connection*/
        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findById);
        ) {

            preparedStatement.setLong(1, toolsId);
            /*4. Execute query*/
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                /*6. Add parsed info into collection*/
                tools = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Tools with id " + toolsId + " not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }

        return tools;
    }

    @Override
    public Tools save(Tools tools) {
        final String insertQuery = "INSERT INTO m_tools (brand, model, personal_number, price, availability)\n" +
                "VALUES (?, ?, ?, ?, ?)";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
             PreparedStatement lastInsertId = connection.prepareStatement("SELECT currval('m_tools_id_seq') as last_insert_id;");
        ) {
            preparedStatement.setString(1, tools.getBrand());
            preparedStatement.setString(2, tools.getModel());
            preparedStatement.setString(3, tools.getPersonalNumber());
            preparedStatement.setDouble(4, tools.getPrice());
            preparedStatement.setBoolean(5, tools.isAvailability());

            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            long insertedToolsId = set.getInt("last_insert_id");
            return findOne(insertedToolsId);

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Tools update(Tools tools) {
        final String updateQuery = "update m_tools set brand = ?, model = ?, personal_number = ?, price = ?, availability = ?" +
                "where id = ?";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
        ) {
            preparedStatement.setString(1, tools.getBrand());
            preparedStatement.setString(2, tools.getModel());
            preparedStatement.setString(3, tools.getPersonalNumber());
            preparedStatement.setDouble(4, tools.getPrice());
            preparedStatement.setBoolean(5, tools.isAvailability());

            preparedStatement.setLong(6, tools.getId());

            preparedStatement.executeUpdate();

            return findOne(tools.getId());

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Tools tools) {
        final String deleteQuery = "delete FROM m_tools " +
                "where id = ?";
        int rowsAffected = 0;

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        ) {
            preparedStatement.setLong(1, tools.getId());


            rowsAffected = preparedStatement.executeUpdate();

            return rowsAffected;

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in delete operation!", e);
        }
    }

    @Override
    public List<Tools> testBatch(Tools tools, Tools toolsTwo) {
        List<Tools> toolsList = new ArrayList<>();
        final String insertBatchQuery = "INSERT INTO m_tools (brand, model, personal_number, price, availability)\n" +
                "VALUES (?, ?, ?, ?, ?)";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertBatchQuery);
        ) {
            connection.setAutoCommit(false);

            preparedStatement.setString(1, tools.getBrand());
            preparedStatement.setString(2, tools.getModel());
            preparedStatement.setString(3, tools.getPersonalNumber());
            preparedStatement.setDouble(4, tools.getPrice());
            preparedStatement.setBoolean(5, tools.isAvailability());

            toolsList.add(tools);
            preparedStatement.addBatch();

            preparedStatement.setString(1, toolsTwo.getBrand());
            preparedStatement.setString(2, toolsTwo.getModel());
            preparedStatement.setString(3, toolsTwo.getPersonalNumber());
            preparedStatement.setDouble(4, toolsTwo.getPrice());
            preparedStatement.setBoolean(5, toolsTwo.isAvailability());

            toolsList.add(toolsTwo);
            preparedStatement.addBatch();

            preparedStatement.executeBatch();
            connection.commit();

            return toolsList;

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    private Tools parseResultSet(ResultSet resultSet) throws SQLException {
        Tools tools = new Tools();

        tools.setId(resultSet.getLong(TOOLS_ID));
        tools.setModel(resultSet.getString(TOOLS_MODEL));
        tools.setBrand(resultSet.getString(TOOLS_BRAND));
        tools.setPersonalNumber(resultSet.getString(TOOLS_PERSONAL_NUMBER));
        tools.setPrice(resultSet.getDouble(TOOLS_PRICE));
        tools.setAvailability(resultSet.getBoolean(TOOLS_AVAILABILITY));

        return tools;
    }
}
