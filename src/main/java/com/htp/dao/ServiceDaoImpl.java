package com.htp.dao;

import com.htp.domain.Service;
import com.htp.domain.Tools;
import com.htp.exceptions.ResourceNotFoundException;
import com.htp.util.DatabaseConfiguration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.htp.util.DatabaseConfiguration.*;
import static com.htp.util.DatabaseConfiguration.DATABASE_PASSWORD;

public class ServiceDaoImpl implements ServiceDao {
    public static DatabaseConfiguration config = DatabaseConfiguration.getInstance();

    public static final String SERVICE_ID = "id";
    public static final String SERVICE_NAME = "service_name";
    public static final String SERVICE_ADDRESS = "service_address";

    @Override
    public List<Service> findAll() {
        final String findAllQuery = "select * from m_service order by id desc";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        List<Service> resultList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);

             PreparedStatement preparedStatement = connection.prepareStatement(findAllQuery)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                resultList.add(parseResultSet(resultSet));
            }

        }
        catch (SQLException e){
            System.out.println(e.getMessage());
        }


        return resultList;
    }

    @Override
    public List<Service> search(String searchParam) {
        final String findAllQueryForPrepared = "select * from m_service where id > ? order by id desc";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        List<Service> resultList = new ArrayList<>();
        /*2. DriverManager should get connection*/
        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findAllQueryForPrepared)) {

            preparedStatement.setLong(1, Long.parseLong(searchParam));

            /*4. Execute query*/
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                /*6. Add parsed info into collection*/
                resultList.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resultList;
    }

    @Override
    public Optional<Service> findById(Long serviceId) {
        return Optional.ofNullable(findOne(serviceId));
    }

    @Override
    public Service findOne(Long serviceId) {
        final String findById = "select * from m_service where id = ? ";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        Service service = null;
        ResultSet resultSet = null;
        /*2. DriverManager should get connection*/
        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(findById);
        ) {

            preparedStatement.setLong(1, serviceId);
            /*4. Execute query*/
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                /*6. Add parsed info into collection*/
                service = parseResultSet(resultSet);
            } else {
                throw new ResourceNotFoundException("Service with id " + serviceId + " not found");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                resultSet.close();
            } catch (SQLException throwables) {
                System.out.println(throwables.getMessage());
            }
        }

        return service;
    }

    @Override
    public Service save(Service service) {
        final String insertQuery = "INSERT INTO m_service (service_name, service_address)\n" +
                "VALUES (?, ?)";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
             PreparedStatement lastInsertId = connection.prepareStatement("SELECT currval('m_service_id_seq') as last_insert_id;");
        ) {
            preparedStatement.setString(1, service.getServiceName());
            preparedStatement.setString(2, service.getServiceAddress());


            preparedStatement.executeUpdate();

            ResultSet set = lastInsertId.executeQuery();
            set.next();
            long insertedService= set.getInt("last_insert_id");
            return findOne(insertedService);

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public Service update(Service service) {
        final String updateQuery = "update m_service set service_name = ?, service_address = ?" +
                "where id = ?";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
        ) {
            preparedStatement.setString(1, service.getServiceName());
            preparedStatement.setString(2, service.getServiceAddress());

            preparedStatement.setLong(3, service.getId());

            preparedStatement.executeUpdate();

            return findOne(service.getId());

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    @Override
    public int delete(Service service) {
        final String deleteQuery = "delete FROM m_service " +
                "where id = ?";
        int rowsAffected = 0;

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        ) {
            preparedStatement.setLong(1, service.getId());


            rowsAffected = preparedStatement.executeUpdate();

            return rowsAffected;

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in delete operation!", e);
        }
    }

    @Override
    public List<Service> testBatch(Service serviceOne, Service serviceTwo) {
        List<Service> servicesList = new ArrayList<>();
        final String insertBatchQuery = "INSERT INTO m_service (service_name, service_address)\n" +
                "VALUES (?, ?)";

        String driverName = config.getProperty(DATABASE_DRIVER_NAME);
        String url = config.getProperty(DATABASE_URL);
        String login = config.getProperty(DATABASE_LOGIN);
        String databasePassword = config.getProperty(DATABASE_PASSWORD);

        /*1. Load driver*/
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            System.out.println("Don't worry:)");
        }

        try (Connection connection = DriverManager.getConnection(url, login, databasePassword);
                /*3. Get statement from connection*/
             PreparedStatement preparedStatement = connection.prepareStatement(insertBatchQuery);
        ) {
            connection.setAutoCommit(false);

            preparedStatement.setString(1, serviceOne.getServiceName());
            preparedStatement.setString(2, serviceOne.getServiceAddress());

            servicesList.add(serviceOne);
            preparedStatement.addBatch();

            preparedStatement.setString(1, serviceTwo.getServiceName());
            preparedStatement.setString(2, serviceTwo.getServiceAddress());

            servicesList.add(serviceTwo);
            preparedStatement.addBatch();

            preparedStatement.executeBatch();
            connection.commit();

            return servicesList;

        } catch (SQLException e) {
            throw new RuntimeException("Some issues in insert operation!", e);
        }
    }

    private Service parseResultSet(ResultSet resultSet) throws SQLException {
        Service service = new Service();

        service.setId(resultSet.getLong(SERVICE_ID));
        service.setServiceName(resultSet.getString(SERVICE_NAME));
        service.setServiceAddress(resultSet.getString(SERVICE_ADDRESS));

        return service;
    }

}
