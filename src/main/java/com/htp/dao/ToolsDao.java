package com.htp.dao;

import com.htp.domain.Tools;

import java.util.List;
import java.util.Optional;

public interface ToolsDao {

    List<Tools> findAll();

    List<Tools> search(String searchParam);

    Optional<Tools> findById(Long toolsId);

    Tools findOne(Long toolsId);

    Tools save(Tools tools);

    Tools update(Tools tools);

    int delete(Tools tools);

    List<Tools> testBatch(Tools tools, Tools toolsTwo);


}
